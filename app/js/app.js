'use strict';

var app = (function(document, $) {
	var docElem = document.documentElement,
		_userAgentInit = function() {
			docElem.setAttribute('data-useragent', navigator.userAgent);
		},
		_fadeSibling = function(elem) {
			var elemClass = '.' + elem.context.classList[0],
				index = elem.index( elemClass );

			$.each($(elemClass), function(i, val) {
				if (i !== index) {
					$(this).css('opacity', 0.5);
				} else {
					$(this).css('opacity', 1);
				}
			});
		},
		_giftImage = function() {
			var list = document.getElementsByClassName('gift');

			if (typeof list !== 'undefined' && list.length > 0) {
				for (var i = 0; i < list.length; i++) {
					var src = list[i].getAttribute('data-image');
					list[i].style.backgroundImage='url(\'' + src + '\')';
				}
			}
		},
		_init = function() {
			$(document).foundation();
            // needed to use joyride
            // doc: http://foundation.zurb.com/docs/components/joyride.html
            $(document).on('click', '#start-jr', function () {
                $(document).foundation('joyride', 'start');
            });
            // Import Fonts
            WebFont.load({
                google: {
                    families: ['Dosis:300,500,800:latin']
                }
            });
            $(document).ready( function () {
			    $('#my-account-table').DataTable();
			});
            $('input[type="radio"].flag, input[type="radio"].gift').on('click', {}, function(event) {
            	event.preventDefault();
            	_fadeSibling( $(this) );
            });

            // datepicker i18n
			$.extend( $.fn.pickadate.defaults, {
				monthsFull: [ 'janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro' ],
				monthsShort: [ 'jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez' ],
				weekdaysFull: [ 'domingo', 'segunda-feira', 'terça-feira', 'quarta-feira', 'quinta-feira', 'sexta-feira', 'sábado' ],
				weekdaysShort: [ 'dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sab' ],
				today: 'hoje',
				clear: 'excluir',
				close: 'fechar',
				format: 'dddd, d !de mmmm !de yyyy',
				formatSubmit: 'dd/mm/yyyy',
				hiddenName: true,
				selectYears: true,
  				selectMonths: true

			});

			$('.datepicker').pickadate();
			$('.datepicker-birth').pickadate({
				selectYears: 30,
				min: new Date(1920,1,1),
				max: true
			});

			/* Random Logo
			----------------> */
				function getRandomNumber(min, max) {
					var n = '';
					n = Math.random() * (max - min) + min;
					return Math.round(n) -1;
				}
				var randomNumber = getRandomNumber(1, 4);

				if (randomNumber === 0) {
					var sortedLogo = 'a';
				} else if(randomNumber === 1){
					var sortedLogo = 'b';
				} else if(randomNumber === 2){
					var sortedLogo = 'c';
				} else if(randomNumber === 3){
					var sortedLogo = 'd';
				};

				$('#instituto-ayrton-senna').addClass(sortedLogo);
				var favicon = '<!-- Favicon --><!--[if IE]><link rel="shortcut icon" href="images/favicon-' + sortedLogo +'.ico"><![endif]--><link rel="icon" href="images/favicon-' + sortedLogo +'.png" sizes="152x152"><!-- IOS --><link rel="apple-touch-icon" href="/images/favicon-' + sortedLogo +'.png"><!-- Windows 8 - Tiles --><meta name="msapplication-TileImage" content="images/favicon-' + sortedLogo +'.png"><meta name="msapplication-TileColor" content="#f9f9f9">';
				$('head').append(favicon);

			/* Select a value
			----------------> */
				function selectValue(button){
					button.on('click', function(event) {
						event.preventDefault();
						if ($(this).is('.active')) {
							if($(this).siblings().is('.active')){
								$(this).removeClass('active');
							}
						} else if(!$(this).is('.active')){
							if($(this).siblings().is('.active')){
								$(this).siblings().removeClass('active');
								$(this).addClass('active');
							} else{
								$(this).addClass('active');
							}
						};
					});
				};
				selectValue($('#monthly .button'));
				selectValue($('#single .button'));

			_userAgentInit();
			_giftImage();
		};
	return {
		init: _init
	};
})(document, jQuery);

(function() {
	app.init();
})();
